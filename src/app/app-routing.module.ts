import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'history',
    pathMatch: 'full'
  },
  {
    path: 'history',
    loadChildren: () => import ('./pages/history-page/history.module').then((module)=> module.HistoryModule)
  },
  {
    path: 'characters',
    loadChildren: () => import ('./pages/characters-page/characters.module').then((module)=> module.CharactersModule)
  },
  {
    path: 'episodes',
    loadChildren: () => import ('./pages/episodes-page/episodes.module').then((module)=> module.EpisodesModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
