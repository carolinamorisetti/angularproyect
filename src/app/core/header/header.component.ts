import { Iheader } from './models/iheader';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public headerNavigation?: Iheader[] = [
   { 
    name:'History',
    path:'history'
  },
  { 
    name:'Characters',
    path:'characters'
  },
  { 
    name:'Episodes',
    path:'episodes'
  }
];

  constructor() { }

  ngOnInit(): void {
  }

}
