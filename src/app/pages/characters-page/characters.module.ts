import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core';
import { CharactersRoutingModule } from './characters-routing.module';
import { HttpClientModule } from '@angular/common/http'
import { CommonModule } from '@angular/common';

import { CharactersComponent } from './characters/characters.component';
import { CharactersService } from './services/characters.service';



@NgModule({
  declarations: [
    CharactersComponent
  ],
  imports: [
    
    CharactersRoutingModule,
    HttpClientModule,
    CommonModule
  ],
  providers:  [
    CharactersService
  ]
})
export class CharactersModule { }