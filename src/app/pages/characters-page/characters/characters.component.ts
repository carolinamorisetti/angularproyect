import { Component, OnInit } from '@angular/core';
import { Icharacters, IcharactersResponse } from './../model/icharacters';
import { CharactersService } from './../services/characters.service';



@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.css']
})
export class CharactersComponent implements OnInit {
  characterList: Icharacters[] = [];

  constructor(private charactersService: CharactersService) { }

  ngOnInit () {

    this.charactersService.getCharacters().subscribe((data: any) => {
      const results: Icharacters[] = data.results;

        const formatterdReseults = results.map(({ id, name, image }) => ({
          id,
          name,
          image,
      }));
        this.characterList = formatterdReseults;
    });
  }
}


