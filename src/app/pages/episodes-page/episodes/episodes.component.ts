import { Component, OnInit } from '@angular/core';
import { Iepisodes } from './../model/iepisodes';
import { EpisodesService } from './../services/episodes.service';


@Component({
  selector: 'app-episodes',
  templateUrl: './episodes.component.html',
  styleUrls: ['./episodes.component.css']
})
export class EpisodesComponent implements OnInit {
  episodesList: Iepisodes[] = [];

  constructor(private episodesService: EpisodesService) { }

  ngOnInit () {

    this.episodesService.getEpisode().subscribe((data: any) => {
      const results: Iepisodes[] = data.results;

      const formattedResults = results.map(({ id, name, image }) => ({
        id,
        name,
        image,
      }));
      this.episodesList = formattedResults;
    });
  }
}
