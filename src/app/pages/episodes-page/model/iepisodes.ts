export interface Iepisodes {
    id: number;
    name: string;
    image: string;
  }
  
  export interface IepisodesResponse {
    info: {
      count: number;
      next: string;
      pages: number;
      prev: string;
    };
    results: Iepisodes[];
  }

