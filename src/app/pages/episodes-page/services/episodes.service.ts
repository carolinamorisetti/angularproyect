import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

const baseUrl = 'https://rickandmortyapi.com/api/'

const episodeUrl = baseUrl + 'episode';

@Injectable()
export class EpisodesService {
  static getEpisode() {
    throw new Error('Method not implemented.');
  }

  constructor(private http: HttpClient) { }

  getEpisode () {
    return this.http.get(episodeUrl);
  }
}
