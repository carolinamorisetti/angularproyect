import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Ihistoryform } from '../../model/ihistoryform';

@Component({
  selector: 'app-history-form',
  templateUrl: './history-form.component.html',
  styleUrls: ['./history-form.component.css']
})
export class HistoryFormComponent implements OnInit {
public historyForm: FormGroup;
public submitted: boolean = false;
  constructor(private formBuilder: FormBuilder) {
    this.historyForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(20)]],
      password: ['', [Validators.required, Validators.maxLength(20)]],
      passwordRepeat: ['', [Validators.required, Validators.maxLength(20)]],

    });
}

  ngOnInit(): void {
  }

  public onSubmit() {
    this.submitted = true;
    if(this.historyForm?.valid) {
      const user: Ihistoryform  = {
        name: this.historyForm.get('name')?.value,
        password: this.historyForm.get('password')?.value,
	      passwordRepeat: this.historyForm.get('passwordRepeat')?.value,

      }
    }
    this.historyForm?.reset();
    this.submitted = false;
  }

}
